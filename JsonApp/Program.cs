﻿using System;
using System.Collections.Generic;
using System.IO;
using Bookshop.Data.Model;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using rox.utils;

namespace JsonApp
{
    internal class Program
    {
        private static void Main(string[] args)
        {
            var book1 = new Book
                {ISBN = "134534", Author = "Kuba Wojewódzki", Title = "Kuba Wojewódzki Show: Dlaczego Java"};
                book1.SelfBook = book1;
            var books = new List<Book>
            {
                new Book {ISBN = "123123123", Author = "Kinga Rusin", Title = "XML - Frameworks",
                    SecAuthor = new Author()
                {
                     Name = "Radosław",
                    Surname = "Majdan",
                    YearOfBirth = 1972
                }},
                book1,
                new Book {ISBN = "1213444", Author = "Andrzej D", Title = "Dlaczego Ja?"}
            };
           
            var str = JsonConvert.SerializeObject(books,Formatting.Indented);
            File.WriteAllText("file.json", str);


            var jsonString = File.ReadAllText("file.json");
            var books2=JsonConvert.DeserializeObject<List<Book>>(jsonString);

            dynamic obj = JArray.Parse(jsonString);

            Console.WriteLine(obj[0].Title);

            Console.WriteLine(Assert.IsEqual(books,books2));
            Console.ReadKey();
        }
    }
}