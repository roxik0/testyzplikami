using System.Collections.Generic;
using System.IO;
using Bookshop.Data.Model;
using FilesExamples;
using Moq;
using Newtonsoft.Json;
using NUnit.Framework;

namespace Tests
{
    public class Tests
    {
        [SetUp]
        public void Setup()
        {
        }

        [Test]
        public void Test1()
        {
            var repo = new Mock<IBookRepository>();
            repo.Setup(p => p.GetAllBooks()).Returns(TestUtils.LoadItemsFromFile<Book>("books.json"));

            var sut=new Order();
           
            var dec=sut.GetTotalAmount(repo.Object.GetAllBooks());
            Assert.AreEqual(100,dec);
            Assert.Pass();
        }
    }

    public class TestUtils
    {
        public static IEnumerable<T> LoadItemsFromFile<T>(string path)
        {
            return JsonConvert.DeserializeObject<IEnumerable<T>>(File.ReadAllText(path));
        } 
    }
}