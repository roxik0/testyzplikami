﻿using System;
using rox.core.console.ui;

namespace FilesExamples
{
    internal class MainMenuWindow : Window
    {
        public MainMenuWindow()
        {
            Console.CursorVisible = false;
            InitializeControls();
        }

        private void InitializeControls()
        {
            var lb = new Label();
            lb.Text = "Witajcie w tym Demie na temat różnych formatów plików";

            var demoMenu = new Menu();
            demoMenu.Top = 4;
            demoMenu.Left = 10;

            var CSVFilesExamples = new MenuItem();
            CSVFilesExamples.Text = "CSV Files";
            demoMenu.AddMenuItem(CSVFilesExamples);


            Controls.Add(lb);
            Controls.Add(demoMenu);
        }
    }
}