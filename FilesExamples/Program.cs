﻿using System;
using System.Collections.Generic;
using Bookshop.Data.Model;

namespace FilesExamples
{
    internal class Program
    {
        private static void Main(string[] args)
        {
            Console.SetWindowSize(Console.LargestWindowWidth / 2, Console.LargestWindowHeight / 2);

            var mainWindow = new MainMenuWindow();
            mainWindow.Run();

            Console.ReadLine();
        }

    }

    public interface IBookRepository
    {
        IEnumerable<Book> GetAllBooks();
    }

    public class Order
    {
        public decimal GetTotalAmount(IEnumerable<Book> list)
        {
            return 0;
        }
    }
}