﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Xml;
using System.Xml.Schema;
using System.Xml.Serialization;
using Bookshop.Data.Model;
using DocumentFormat.OpenXml;
using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Wordprocessing;
using OpenXmlPowerTools;
using rox.utils;

namespace XMLExamples
{
    internal class Program
    {
        private static void Main(string[] args)
        {


            WordprocessingDocument doc= WordprocessingDocument.Open("example.docx",true);
            SimplifyMarkupSettings settings = new SimplifyMarkupSettings
            {
                //RemoveProof = true,
                NormalizeXml = true
            };
            MarkupSimplifier.SimplifyMarkup(doc, settings);
            var body=doc.MainDocumentPart.Document.Body;

            var paragraph = new Paragraph();
            var run = new Run();
            var text = new Text("Ten kurs naprawdę uczy jak edytować worda WOW WOW");

            run.Append(text);
            paragraph.AppendChild(run);

           // body.AppendChild(paragraph);

            var texts=body.Descendants<Text>();
            foreach (var text1 in texts)
            {
                if (text1.Text.Contains("[PLACEHOLDER]"))
                {
                    text1.Text = text1.Text.Replace("[PLACEHOLDER]", "Codementors są super");
                }
            }



            doc.Save();
            doc.Close();














            var books = new List<Book>
            {
                new Book {ISBN = "123123123", Author = "Kinga Rusin", Title = "XML - Frameworks",
                    SecAuthor = new Author()
                    {
                        Name = "Radosław",
                        Surname = "Majdan",
                        YearOfBirth = 1972
                    }},
                new Book {ISBN = "134534", Author = "Kuba Wojewódzki", Title = "Kuba Wojewódzki Show: Dlaczego Java"},
                new Book {ISBN = "1213444", Author = "Andrzej D", Title = "Dlaczego Ja?"}
            };
            var authors = new List<Author>
            {
                new Author {Name = "Kinga", Surname = "Rusin", YearOfBirth = 1971},
                new Author {Name = "Kuba", Surname = "Wojewódzki", YearOfBirth = 1963},
                new Author {Name = "Andrzej", Surname = "D", YearOfBirth = 1972}
            };
            using (var authFile = File.CreateText("authors.xml"))
            {
                XmlSerializer<List<Author>>.Serialize(authFile.BaseStream, authors);
            }

            var ser = new XmlSerializer(typeof(List<Book>));
            var file = File.CreateText("file.xml");
            ser.Serialize(file, books);
            file.Close();


            using (var myFile = File.OpenRead("bookstore.xml"))
            {
                var xmlSerializer = new XmlSerializer(typeof(bookstore));
            //    var xxx = xmlSerializer.Deserialize(myFile) as bookstore;
           //     Console.WriteLine(xxx.book[0].title.Value);
            }


            using (var fileHandler = File.OpenRead("file.xml"))
            {
                var books2 = ser.Deserialize(fileHandler) as List<Book>;
                Console.WriteLine(Assert.IsEqual(books, books2));
            }


            var document = new XmlDocument();
            document.Load("bookstore.xml");
            using (var xsdFile = File.OpenRead("bookstore.xsd"))
            {
                document.Schemas.Add(XmlSchema.Read(xsdFile, BooksSettings_ValidationEventHandler));

            }
            document.Validate(BooksSettings_ValidationEventHandler);

            var root = document.DocumentElement;
            var node = root.SelectNodes(@"//bookstore/book/price");

            foreach (var n in node)
            {
                var nn = n as XmlNode;
                var money = Convert.ToDecimal(nn.InnerText.Replace(".", ","));
                money = 0.9m * money;
                nn.InnerText = money.ToString();
            }

            document.Save("bookstoreNew.xml");

          
         


            Console.ReadKey();
        }

        private static void BooksSettings_ValidationEventHandler(object sender, ValidationEventArgs e)
        {
        }
    }
}