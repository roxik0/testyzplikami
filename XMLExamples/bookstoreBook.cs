﻿using System;
using System.ComponentModel;
using System.Xml.Serialization;

namespace XMLExamples
{
    /// <remarks />
    [Serializable]
    [DesignerCategory("code")]
    [XmlType(AnonymousType = true)]
    public class bookstoreBook
    {
        private string authorField;

        private string categoryField;

        private decimal priceField;

        private bookstoreBookTitle titleField;

        private ushort yearField;

        /// <remarks />
        public bookstoreBookTitle title
        {
            get => titleField;
            set => titleField = value;
        }

        /// <remarks />
        public string author
        {
            get => authorField;
            set => authorField = value;
        }

        /// <remarks />
        public ushort year
        {
            get => yearField;
            set => yearField = value;
        }

        /// <remarks />
        public decimal price
        {
            get => priceField;
            set => priceField = value;
        }

        /// <remarks />
        [XmlAttribute]
        public string category
        {
            get => categoryField;
            set => categoryField = value;
        }
    }
}