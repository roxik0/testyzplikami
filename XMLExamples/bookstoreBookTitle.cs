﻿using System;
using System.ComponentModel;
using System.Xml.Serialization;

namespace XMLExamples
{
    /// <remarks />
    [Serializable]
    [DesignerCategory("code")]
    [XmlType(AnonymousType = true)]
    public class bookstoreBookTitle
    {
        private string langField;

        private string valueField;

        /// <remarks />
        [XmlAttribute]
        public string lang
        {
            get => langField;
            set => langField = value;
        }

        /// <remarks />
        [XmlText]
        public string Value
        {
            get => valueField;
            set => valueField = value;
        }
    }
}