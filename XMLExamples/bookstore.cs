﻿using System;
using System.ComponentModel;
using System.Xml.Serialization;

namespace XMLExamples
{
    /// <remarks />
    [Serializable]
    [DesignerCategory("code")]
    [XmlType(AnonymousType = true)]
    [XmlRoot(Namespace = "", IsNullable = false)]
    public class bookstore
    {
        private bookstoreBook[] bookField;

        /// <remarks />
        [XmlElement("book")]
        public bookstoreBook[] book
        {
            get => bookField;
            set => bookField = value;
        }
    }
}