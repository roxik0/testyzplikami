﻿using System.IO;
using System.Xml.Serialization;

namespace XMLExamples
{
    public class XmlSerializer<T> where T : class
    {
        public static void Serialize(Stream stream, T obj)
        {
            var xmlSerializer = new XmlSerializer(typeof(T));
            xmlSerializer.Serialize(stream, obj);
        }

        public static T Deserialize(Stream stream)
        {
            var xmlSerializer = new XmlSerializer(typeof(T));
            return xmlSerializer.Deserialize(stream) as T;
        }
    }
}