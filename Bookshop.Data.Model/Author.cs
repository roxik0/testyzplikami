﻿namespace Bookshop.Data.Model
{
    public class Author
    {
        public string Name { get; set; }
        public string Surname { get; set; }
        public int YearOfBirth { get; set; }

        public override bool Equals(object obj)
        {
            var author = obj as Author;
            return author != null &&
                   Name == author.Name &&
                   Surname == author.Surname &&
                   YearOfBirth == author.YearOfBirth;
        }
    }
}