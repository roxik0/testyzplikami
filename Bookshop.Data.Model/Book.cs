﻿using System.Xml.Serialization;

namespace Bookshop.Data.Model
{
    public class Book
    {
        [XmlAttribute(AttributeName = "ISBN")] public string ISBN { get; set; }

        [XmlElement(ElementName = "Tytuł")] public string Title { get; set; }

        public string Author { get; set; }

        public Author SecAuthor { get; set; }
        public Book SelfBook { get; set; }


        public override bool Equals(object obj)
        {
            var book = obj as Book;
            return book != null &&
                   ISBN == book.ISBN &&
                   Title == book.Title &&
                   Author == book.Author;
        }
    }
}