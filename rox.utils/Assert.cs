﻿using System.Collections.Generic;

namespace rox.utils
{
    public class Assert
    {
        public static bool IsEqual<T>(List<T> listA, List<T> listB)
        {
            if (listA.Count != listB.Count) return false;
            for (var i = 0; i < listA.Count; i++)
            {
                var objA = listA[i];
                var objB = listB[i];
                if (!objA.Equals(objB)) return false;
            }

            return true;
        }
    }
}