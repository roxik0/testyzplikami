﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;

namespace CsvConsoleExample
{
    public class CsvFileHandler
    {
        public static char _separator = CultureInfo.CurrentCulture.TextInfo.ListSeparator[0];

        public void Dump<T>(string filePath, List<T> books) where T : class
        {
            var streamWriter = File.CreateText(filePath);
            var type = typeof(T);
            var properties = type.GetProperties();
            streamWriter.WriteLine(string.Join(_separator, properties.Select(c => c.Name)));
            foreach (var book in books)
            {
                var values = properties.Select(c => c.GetValue(book).ToString()).ToArray();
                streamWriter.WriteLine(string.Join(_separator, values));
            }

            streamWriter.Close();
        }

        public List<T> LoadBooks<T>(string filePath)
        {
            var result = new List<T>();

            var sr = new StreamReader(filePath);
            var stringLine = sr.ReadLine();
            var headerValues = stringLine.Split(_separator).ToList();
            var type = typeof(T);
            var properties = type.GetProperties();
            var dict = properties.Select(c => new {Property = c, Index = headerValues.IndexOf(c.Name)})
                .ToDictionary(c => c.Property, c => c.Index);


            while (!sr.EndOfStream)
            {
                stringLine = sr.ReadLine();
                var stringItems = stringLine.Split(_separator);
                var obj = Activator.CreateInstance<T>();

                foreach (var property in properties)
                {
                    var index = dict[property];
                    property.SetValue(obj, stringItems[index]);
                }

                result.Add(obj);
            }

            return result;
        }
    }
}