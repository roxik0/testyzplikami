﻿using System;
using System.Collections.Generic;
using Bookshop.Data.Model;
using rox.utils;

namespace CsvConsoleExample
{
    internal class Program
    {
        private static void Main(string[] args)
        {
            var books = new List<Book>
            {
                new Book {ISBN = "123123123", Author = "Kinga Rusin", Title = "XML - Frameworks"},
                new Book {ISBN = "134534", Author = "Kuba Wojewódzki", Title = "Kuba Wojewódzki Show: Dlaczego Java"},
                new Book {ISBN = "1213444", Author = "Andrzej D", Title = "Dlaczego Ja?"}
            };
            new CsvFileHandler().Dump("books.csv", books);

            var books2 = new CsvFileHandler().LoadBooks<Book>("books.csv");

            Console.WriteLine(Assert.IsEqual(books, books2));

            var authors = new List<Author>
            {
                new Author {Name = "Kinga", Surname = "Rusin", YearOfBirth = 1971},
                new Author {Name = "Kuba", Surname = "Wojewódzki", YearOfBirth = 1963},
                new Author {Name = "Andrzej", Surname = "D", YearOfBirth = 1972}
            };

            new CsvFileHandler().Dump("authors.csv", authors);
            Console.ReadKey();
        }
    }
}