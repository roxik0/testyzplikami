﻿using System;
using System.Collections.Generic;

namespace rox.core.console.ui
{
    public class Menu : Control
    {
        public List<MenuItem> MenuItems { get; set; } = new List<MenuItem>();

        public override void Draw(int parentLeft, int parentTop)
        {
            Console.SetCursorPosition(parentLeft + Left, parentTop + Top);


            for (var i = 0; i < MenuItems.Count; i++)
            {
                var item = MenuItems[0];
                item.Left = Left;
                item.Top = i * 3;
                item.Draw(parentLeft + Left, parentTop + Top);
            }
        }

        public void AddMenuItem(MenuItem mi)
        {
            MenuItems.Add(mi);
        }
    }
}