﻿using System;
using System.Collections.Generic;

namespace rox.core.console.ui
{
    public class Window
    {
        public IList<Control> Controls { get; set; } = new List<Control>();

        public void Run()
        {
            Console.BackgroundColor = ConsoleColor.Blue;
            Console.ForegroundColor = ConsoleColor.White;
            Console.Clear();

            foreach (var control in Controls) control.Draw(0, 0);
        }
    }
}