﻿using System;

namespace rox.core.console.ui
{
    public class Label : Control
    {
        public string Text { get; set; }

        public override void Draw(int parentLeft, int parentTop)
        {
            Console.SetCursorPosition(parentLeft, parentTop);
            Console.Write(Text);
        }
    }
}