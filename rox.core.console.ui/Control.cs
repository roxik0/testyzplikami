﻿namespace rox.core.console.ui
{
    public abstract class Control
    {
        public int Top { get; set; }
        public int Left { get; set; }
        public int Height { get; set; }
        public int Width { get; set; }
        public abstract void Draw(int parentLeft, int parentTop);
    }
}