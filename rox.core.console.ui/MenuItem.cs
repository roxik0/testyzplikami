﻿using System;

namespace rox.core.console.ui
{
    public class MenuItem : Control
    {
        public MenuItem()
        {
            Width = 15;
            Height = 3;
        }

        public string Text { get; set; }

        public override void Draw(int parentLeft, int parentTop)
        {
            var oldColor = Console.BackgroundColor;
            Console.BackgroundColor = ConsoleColor.Cyan;
            Console.SetCursorPosition(parentLeft + Left, parentTop + Top);
            for (var j = 0; j < Height; j++)
            {
                Console.SetCursorPosition(parentLeft + Left, parentTop + Top + j);
                Console.Write(new string(' ', Width));
            }

            Console.SetCursorPosition(parentLeft + Left + 1, parentTop + Top + 1);
            Console.Write(Text);
            Console.BackgroundColor = oldColor;
        }
    }
}