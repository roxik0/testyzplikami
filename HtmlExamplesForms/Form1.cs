﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace HtmlExamplesForms
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            HttpClient hc = new HttpClient();
          
            var str = hc.GetStringAsync(@"http://dilbert.com/strip/2019-11-09").Result;

            HtmlAgilityPack.HtmlDocument doc = new HtmlAgilityPack.HtmlDocument();
            doc.LoadHtml(str);



            var htmltext = doc.DocumentNode.SelectNodes("//*[@class=\"img-responsive img-comic\"]")[0].Attributes["src"].Value ;
           var img= Image.FromStream(hc.GetStreamAsync("http:"+htmltext).Result);
            pictureBox1.Image=img;
        }

        public byte cell(int val)
        {
            if (val > 255) return 255;
            if (val < 0) return 0;
            return (byte)val;
        }
        private void button2_Click(object sender, EventArgs e)
        {
            var m= new []
            {
                new []{-1,0,-1},
                new [] { 0, 20, 0 },
                new [] {- 1, 0, -1 }

            };


            Bitmap bmp= new Bitmap("example.jpg");
            for (int i = 0; i < bmp.Width; i++)
            {
                for (int j = 0; j < bmp.Height; j++)
                {
                    int val = 0;
                    int valWag = 0;
                    for (int k = 0; k < 3; k++)
                    {
                        for (int l = 0; l < 3; l++)
                        {
                           var waga= m[k][l];
                            valWag += waga;
                            var pixel = GetPixel(bmp, i - 1 + k, j - 1 + l, bmp.Width, bmp.Height);
                            val = waga*pixel;
                        }
                    }

                    val = val / valWag;

                    var color=bmp.GetPixel(i, j);
                    
                    color=Color.FromArgb(color.A,cell(val), cell(val), cell(val));
                    bmp.SetPixel(i,j,color);
                }
            }





            pictureBox1.Image = bmp;
           

        }

        private byte GetPixel(Bitmap bmp, int i, int j,int w,int h)
        {
            if (i < 0 || i >= w)
            {
                return 0;
            }
            if (j < 0 || j >= h)
            {
                return 0;
            }

            var col = bmp.GetPixel(i, j);
            var b= (col.R + col.G + col.B)/3;
            return (byte)(b);
        }
    }
}
